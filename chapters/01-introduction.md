# Introduction
## Context and Motivation
Cryptocurrencies are inherently decentralised peer to peer networks without any central authority which provides a verifiable, agreed upon state that can monitor or manage the network. As a result, there are no built-in means to analyse the network. In a centralised system, gathering network statistics is a remarkably straightforward process where all the necessary data is already stored and organised in a central database for that purpose. In a decentralised network, this is not the case.

In a centralised network, each node is aware of the network topology and can effortlessly get access to network statistics as data is regularly collected and stored in a centralised database for that purpose. Decentralised networks differ as all nodes are entirely unaware of the network topology and there is no centralised server that stores and maintains data about the network.

To properly analyse a decentralised network, there needs to be a tool that can request information from multiple blockchain nodes and aggregate it into a central database for analysis. Due to the decentralised nature of the network, a single node would not be sufficient for this tool as it does not have access to the entire network and can only see a portion of that network. Multiple nodes spread throughout the network can obtain a much more accurate picture of the network. This tool would need to be flexible enough to be used on multiple blockchains which each have their requirements and implementations. The gathered data should then formatted in the same way for each blockchain and then stored in a centralised database where it can be analysed and displayed using graphs. The raw data itself is useless until it is processed and analysed.

It is entirely possible that there exists a tool to analyse decentralised networks. However, there are few open source tools available to the public that can give a clear picture of these networks and they focus on network statistics and not the network topology.

The health and level of decentralisation of the network is important as these networks hold vast sums of money which they claim to store safely and securely using the fact that these networks are decentralised. However it is difficult to accurately determine how decentralised these networks are. Therefore the purpose and motivation for this thesis is to provide an open source tool that can be used to collect, interpret and analyse data from blockchain networks. This data will then be used to gain a better uderstanding about the topology of these networks which could potentially allow for the identification of isolated nodes, bottlenecks and more.

## Goal of the Thesis
This thesis aims to determine the topology of the decentralised peer-to-peer network of a cryptocurrency and analyse how it is divided between different hosting services. No single entity can give a picture of the network since the protocol for each blockchain is decentralised.

A tool needs to be built to be able to observe the network topology to analyse the resiliency of the network by giving an approximate image of the network. The approximation of the topology will be determined by deploying multiple nodes across each blockchain network in geographically different locations and analysing all observable neighbouring nodes and transactions. Each node should be able to discover a different set of peers depending on geographic positioning. With this data, an analysis can be conducted on the decentralised state of the network and the topology of these networks.

## Metrics
To gain a better understanding of the network topology, data for the following metrics will be collected, processed and analysed.

The degree of decentralisation of a cryptocurrency across the internet can be determined by collecting the remote addresses of observable peers. The remote address can then used to extract the \gls{asn} for that IP. An \gls{as} "is a group of IP networks run by one or more network operator(s) with a single, clearly defined routing policy." \cite{asnumber} Each \gls{as} has a unique number by which it is identified. By obtaining the overall number of \gls{as}s and the distribution of nodes across those \gls{as}s, it is then possible to identify clusters of nodes running off of the same web service or organisation.

The fragmentation of the network is the repartition of nodes running different software implementations or versions. This is determined by observing the software implementations and versions being run by observable peers. When a new update is released, not all nodes update their software at the same time. This metric will give a general idea of how reliable the network is and whether the majority of nodes do update to the latest available patch or not.

The time that it takes for transactions to propagate through the network. Different nodes located in geographically different places may be able to observe the same transactions at different times. This metric will help to understand how quickly and efficiently transactions can propagate through the network, as well as whether adding nodes in specific locations can help to optimise the network.

## Related Work
A tool that can observe and analyse a blockchain network is not a groundbreaking project and has most likely been done countless times before. However, there are few open source tools that available to the public that can be quickly deployed on a node to extract and analyse data regarding the network. Moreover, it is even less likely that such tools are compatible with multiple blockchain networks and able to provide cross blockchain analytics.

The Node Explorer tool makes use of multiple full nodes which are participating in the network means that in principle they do not act as miners but still validate transactions. The tool then runs on top of this network extracting data from the node. This can be compared to a blockchain explore as they make use of multiple full nodes to connect to the network and a script to extract transactions, blocks and addresses and make them available over the web 2.0 to end users in any browser.

For instance, Etherscan \cite{etherscan} and Blockchain \cite{blockchainexp} which are Ethereum and Bitcoin block explorers allow users to search for transactions, blocks and addresses present in their respective network. Both of these services provide Developer API that allows users to interact with their system. They also provide some charts and statistics regarding the change in price over time, the growth of transactions, units in circulation and other similar statistics.

## Thesis Outline
This thesis begins in Chapter 1 with the introduction. The introduction includes the context, motivation, goal, metrics and related work.

Chapter 2 contains a review of all the terms and concepts regarding blockchain technology. Including a brief history of cryptocurrencies, the differences between centralised, distributed and decentralised networks. Then an introduction to the blockchain and how nodes in the network can achieve consensus, what functions nodes can perform, how mining works and how the distributed ledger is maintained. Finally the structure of blocks and transactions, Merkle trees and the difference between soft and hard forks and their variations.

Chapter 3 goes into P2P network structure as well as some pros and cons of both P2P networks and client-server networks and describing the network architecture present in blockchain networks, starting with Bitcoin. The structure of the Bitcoin network is briefly described before jumping into the massages used by bitcoin's protocol followed by sequence diagrams to display the communication between nodes. Followed by a description of the Ethereum network structure and both the DevP2P and Ethereum Sub Protocols. The various messages are explained and sequence diagrams of how nodes communicate with one another. Finally, the chapter goes into P2P security concerns.

Chapter 4 describes the implementation of the Node Explorer tool starting with a context diagram representing the tools system, including an explanation of all the parts involved. Followed by a description of the general implementation of the tool, including the packages, database setup, tickers, transactions and peers.

Chapter 5 and 6 continue describing the implementation of the tool but more specifically the implementation of Ethereum and Bitcoin. This includes the node setup, a look at the code used to retrieve peers and transactions and finally the issues encountered during the development process.

 Chapter 7 contains the visualisations of the data that has been collected. The graphs for each metric are described, and an analysis of the results is given.

Chapter 8 a conclusion of the work presented in this thesis with a summary of what has been accomplished.

Finally, Chapter 9 covers the work that still needs to be done on this project as it is still under active development.
