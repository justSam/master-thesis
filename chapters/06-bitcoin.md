# Bitcoin Implementation
Bitcoin is the first digital currency to be fully decentralised and thus not requiring a central bank or assistance from third parties. Satoshi Nakamoto's goal as stated in Bitcoins white paper was to build "an electronic payment system based on cryptographic proof instead of trust, allowing any two willing parties to transact directly with each other without the need for a trusted third party."\cite{btcwhitepaper}

Bitcoin was created as an alternative currency with a fixed supply of available coins, 21'000'000 coins to be exact. These coins can be exchanged between people without it being linked to their personal identity making it pseudo-anonymous as a user can be associated with more than one public address. The exchange of currency through transactions requires a transaction fee, which goes to the miner that is processing and validating the transaction. The transaction fee can be set by the creator of the transaction. However the higher the transaction fee is set, the faster a transaction will be included in a block. Transactions with low transaction fees may never be included.

## Node
In Bitcoin's case, there aren't different kinds of synchronisations to avoid downloading the full blockchain. Instead, the node can either be a full node or what are called thin clients.
A full node downloads a copy of the entire blockchain on the machine, including all transactions which can all be verified by the node preventing double spending and other potential attacks.

A \gls{spv} node on the other hand only downloads transaction headers and therefore "[...]can't check the transaction for [it]self, but by linking it to a place in the chain, [the client] can see that a network node has accepted it, and blocks added after it further confirms the network has accepted it."\cite{btcwhitepaper}

The tool currently runs on a Full Bitcoin node which is part of Bitcoin Core, called `bitcoind` which implements the Bitcoin protocol for \gls{rpc} use. It is run quite simply with the following command.
```
bitcoind -daemon
```
Unlike with Ethereum, `bitcoind` runs in headless mode and therefore can run continuously without having to worry about closing the SSH session.

Alternatively, to run the `bitcoind` full node, there is a full node implementation of Bitcoin written in Go called btcd which could be used instead. However considering that `bitcoind` is Bitcoin Core's implementation, it made more sense to start there.
btcd also provides a `rpcclient` which is the client used by the tool to connect to the node and do all of it is \gls{rpc} calls. Moreover, sany commands implemented for `bitcoind` should also work on the btcd full node.

A full node is running instead of a thin client primarily because there are no Go compatible clients to simplify the connection to the thin client. There is at least one written in C which could be a good alternative considering that Go and C can be cross-compatible.

All required parameters for connecting to the node should be added to the configuration file so that it is then possible to connect to the node through btcd's `rpcclient` using the code below.
```
connCfg := &rpcclient.ConnConfig{
    Host:            config.Connect\gls{rpc}.Host,
    User:             config.Connect\gls{rpc}.RpcUser,
    Pass:            config.Connect\gls{rpc}.RpcPassword,
    HTTPPostMode:    config.Connect\gls{rpc}.HTTPPostMode,
    DisableTLS:        config.Connect\gls{rpc}.DisableTLS,
}

client, err := rpcclient.New(connCfg, nil)
core.CheckErr("Could not create rpc client: %v", err)
```
The \gls{rpc} user and \gls{rpc} password match those set within the configuration file `bitcoin.conf` \ref{lst:bitcoinconf} located on the machine running the node. This configuration file contains all the settings with which to run the node, optionally the parameters can be passed manually when running `bitcoind -daemon`.

## Transactions
With the node running, similarly to Ethereum, the code connects to the \gls{rpc} client and to the database using the information provided in the configuration file. Tickers are set here as well so that transactions can be polled.
Bitcoin does not have a filter like Ethereum to collect all incoming transactions. Instead, a list of all unconfirmed and pending transactions can be found in the memory pool. Transactions sit in the memory pool until they are included into a block or are removed if they stay in there too long.

```
transactions, err := client.GetRawMempool()
core.CheckErr("Could not fetch tx from mempool: %v", err)
```
With the above code, it is possible to fetch a list of all transactions stored in the local memory pool of the node.
The time at which these transactions were observed is assigned to a variable and passed to the function---`TxsFromBitcoin`---that moves this data into a `struct` of generic type `Transaction` along with the collected hashes and node ID.

```
func TxsFromBitcoin (btcTxs []*chainhash.Hash, timeObs int64, nodeID int) []types.Transaction {
    var transactions []types.Transaction
    for _, btcTx := range btcTxs {
        transactions = append(transactions, TxFromBitcoin(btcTx, core.Pending, timeObs, nodeID))
    }
    return transactions
}
```
The function above iterates over the list of pending transaction hashes, and normalises each transaction into the generic type `Transaction` by passing the transaction hash, observed time, status and node ID, just like with Ethereum the status is by default set to `Pending`.
The function below is where the transactions are converted to type `Transaction`.
```
func TxFromBitcoin (btcTx *chainhash.Hash, status core.Status, timeObs int64, nodeID int) types.Transaction {
    return types.Transaction{btcTx.String(), timeObs, status, "BTC", nodeID}
}
```
Once the transactions are normalized, they are then inserted into the database. This is done for all observed pending transactions.

Just like with Ethereum, a variable `nextBlock` holds the block number of the latest block which is initialised to `0`.
A Ticker is declared for the portion of code responsible for fetching the blocks so that this may occur concurrently with the code responsible for collecting the incoming pending transactions.
The latest block is fetched, and the block number is saved to `latestBlockNumber`.

```
latestBlockNumber, err := client.GetBlockCount()
core.CheckErr("Could not fetch next block:", err)

if nextBlock == 0 {
    fmt.Print("Getting latest block...")
    nextBlock = latestBlockNumber
}
```
There is no built-in \gls{rpc} command to retrieve the block number of the latest block in the longest chain. Therefore it is possible to use `GetBlockCount` which returns the length of the chain which is the same as its position in the chain determines the block number.
If `nextBlock` is still `0` then the `latestblockNumber` is assigned to it.

```
for nextBlock <= latestBlockNumber {
                fmt.Printf("Getting block %d...", nextBlock)
                block := GetBTCBlock(client, nextBlock)
                fmt.Printf(" block %s, %d txs\n", block.Number, len(block.Transactions))

                timeComp, err := strconv.ParseInt(block.Timestamp, 0, 64)
                core.CheckErr("Can't convert timestamp:", err)
                for _, hash := range block.Transactions {
                    database.UpdateTransaction(hash, config.Node.Currency, core.Success, timeComp, config.Node.ID)
                }
                nextBlock = block.GetNumber() + 1
            }
```

While `nextBlock <= latestBlockNumber` the list of transactions from the `nextBlock` are extracted. The condition of the `for` loop ensures that if the program finds itself a couple of blocks behind, it is still able to retrieve and update all of the included transactions.

To extract the data from the block, the function `GetBTCBlock()` uses `nextBlock` which contains the height of the next block to fetch the hash of the block using `GetBlockHash()`. This returns the hash of the block which can then be used to extract the data with `getBlockVerbose()`.
```
func GetBTCBlock(client *rpcclient.Client, blockHeight int64) types.Block{
    hash, err := client.GetBlockHash(blockHeight)
    core.CheckErr(fmt.Sprintf("Can't get block hash for %d:", blockHeight), err)
    block, err := client.GetBlockVerbose(hash)
    blockData := normalize.BlockFromBitcoin(block)
    return blockData
}
```
The data of the block returned by the function is then normalized so that the data can be converted to the `Block` type which contains the fields `Number`, `Timestamp` and `Transactions`.

```
func BlockFromBitcoin (btcBlock *btcjson.GetBlockVerboseResult) types.Block {
    return  types.Block{strconv.FormatInt(btcBlock.Height, 10), strconv.FormatInt(btcBlock.Time, 10), btcBlock.Tx}
}
```
With the block data, the `for` loop continues and each transaction in the collected list of transactions is updated with the timestamp of the block and the status. Bitcoin does not use transaction statuses in the same way Ethereum does. Transactions are either pending, unconfirmed or completed, so when transactions are included in a block, they are marked as successful.

## Peers
Collecting peers in Bitcoin unlike in Ethereum can be done using built-in \gls{rpc} commands.
```
btcPeers, err := client.GetPeerInfo()
core.CheckErr("Could not fetch peers: %v", err)
```
The `GetPeerInfo` command returns the data about each connected peer present in the network as a `GetPeerInfoResult` type which is a btcd generic type. The data is assigned to `btcPeers` which is then passed as a parameter of `PeersFromBitcoin` function that normalises the peers.
```
peers := types.PeersFromBitcoin(btcPeers)
```
The peers are normalised below in almost the same way as is done for Ethereum. The remote address extracted from the network is the IP followed by the Port so the to are separated and the IP stored.

For each peer, the IP address is used to fetch the \gls{as} number and stored to a variable. The data for the peer is then normalised into the generic type `Peer`.
```
func PeerFromBitcoin (btcPeer btcjson.GetPeerInfoResult, timeObs int64, nodeID int) types.Peer {
    remoteAddress, _, err := net.SplitHostPort(btcPeer.Addr)
    core.CheckErr("Failed to remove port: ", err)

    ASNumber := database.CheckExistingAS(remoteAddress)
    return types.Peer{fmt.Sprint(btcPeer.Version, ", ", btcPeer.SubVer), remoteAddress, ASNumber, timeObs, "BTC", nodeID}
}

func PeersFromBitcoin (btcPeers []btcjson.GetPeerInfoResult, timeObs int64, nodeID int) []types.Peer {
    var peers []types.Peer
    for _, btcPeer := range btcPeers {
        peers = append(peers, PeerFromBitcoin(btcPeer, timeObs, nodeID))
    }
    return peers
}
```
The list of normalised peers is then returned and they are added to the database if there is no conflict and if there is then nothing is done.

## Issues encountered
Since I began this project by running a full Ethereum node, I had a better idea of what had to be done. The main difference is that Ethereum has significantly better documentation which details all the different options available and what they are used for.

With Bitcoin this process required more trial and error and despite being able to get the node running I was unable to send \gls{rpc} commands using the command line interface. After a bit of research I realised that this was since I was missing a Bitcoin configuration file that needed to be copied from the Bitcoin wiki \cite{btcconf}. The configuration file included a `rpcuser` and `rpcpassword` which were essential for connecting to `bitcoind` through \gls{rpc}. Once I modified the configuration file with the correct credentials I was finally able to start making \gls{rpc} calls and collect data.

Once the node was up and running it continued to run without issues and did not interrupt when I connected and disconnected via \gls{ssh}
