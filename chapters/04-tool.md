# Blockchain Network Explorer
## System
\begin{figure}[ht!]
\centering
\label{fig:contextdiagram}
\includegraphics[scale=0.4]{./images/context-diagram.png}
\caption{Overview of the system.}
\label{fig:contextdiagram}
\end{figure}

In figure \ref{fig:contextdiagram} above is a visual representation of the system which is comprised of multiple nodes, multiple instances of the Node Explorer tool written in Go, a \gls{psql} database, the analysis tool also written in Go with a REST API with some client-side code written in Go and JavaScript to display plots.

The blockchain nodes are displayed as Ethereum and Bitcoin full nodes which are connected to their respective networks. The Ethereum node uses `geth` to connect to its network and the Bitcoin node uses `bitcoind`. For each blockchain node, the appropriate node software must be used to be able to connect to its respective network.

The Node Explorer is the tool that has been implemented to be able to connect to the blockchain networks and extract observable data. So far the tool connects to the Bitcoin and Ethereum networks using \gls{rpc}. However, the tool is written in a modular fashion such that the logic to talk to nodes from other cryptocurrencies can be implemented easily, regardless of those nodes' communication protocols. Node Explorer communicates with the blockchain nodes to extract data which is then normalised and inserted into a centralised database using \gls{psql}. The decision to make the Node Explorer tool distributed rather than centralised was to ensure the systems reliability. If the Node Explorer tool were running on a centralised machine then if that machine crashes the data collection would be interrupted for all nodes rather than just one.

The centralised database collects the normalised data from each blockchain node. This data, from various cryptocurrencies, are all added to the same tables which facilitate its analysis. Using \gls{psql} the analysis script can extract and process the data then convert it into JSON format which is then used to display plots as a static web page. A REST-API will be added soon to update the previously mentioned plots automatically and to let other tools and services use the data to gain a better understanding of the blockchain networks. There are various patterns on a centralised database that can guarantee that if the machine crashes, no data would be lost.

## Security
An overview of security concerns that arise in peer-to-peer networks was given in Chapter 3. In this thesis the only security concern to note is related to the security of the network as a whole. As well as the reliability of the data that the nodes are receiving, which is why the tool is only using data observed by nodes within the system.

## Implementation
The tool is intended to run on the same machine as the blockchain node but it could also be run on a different machine and connect to the node remotely. The tool extracts pending transactions and a list of connected peers that the node can observe. Nodes that are located in geographically different places may be able to observe transactions or peers that other nodes do not.

The tool itself is done entirely with Go, a new programming language that was created by Google with Cloud computing in mind and was then released as an open source programming language. It is a simple language that is easy to learn and understand making it great to maintain. Go is a compiled language similarly to lower level languages like C/C++ and therefore can be run as a binary significantly increasing performance.

Data visualisation is done using a combination of JavaScript and HTML by using a tool called Plotly which creates graphs using the data provided in the database.

Each currency has it is own different client interface that enables a user to run their node. This means that for each cryptocurrency that is added, the tool will need to run the specific commands for that currencies client. It has been designed such that a new cryptocurrency can easily be added by just creating a new file and modifying the currency specific commands.  Some currencies have more than one client interface with different implementations. Support for both may require more work depending on how much they differ.

### Packages
This tool makes use of a few non-standard go packages. This includes the node client packages---that allow it to connect to a node---that is different for each blockchain implemented.

To avoid hard-coding values as much as possible, all of the node's settings can be specified in an easy to read the configuration file. The Language used is called \gls{toml} which is a simplified configuration file format that has support for many different programming languages.

`pq` which is a \gls{psql} driver for Go's `database/sql` package, which is used to connect/disconnect and query the \gls{psql} database.

Ammario's `ipisp` package \cite{ipisp}, an "IP to ISP library utilizing team cymru's IP to \gls{asn} service".\cite{ipisp} This package was used to extract the \gls{as} number from remote addresses.

### Database
The data that is collected by the tool needs to be stored somewhere. Therefore a dedicated server was set up to act as the centralised database for all the gathered data. The database management system that was used is \gls{psql}, which is open source and free to use with a strong community and a great deal of documentation and tutorials. More importantly, it handles concurrency which allows it to handle many tasks efficiently. The use of \gls{psql} was a technical requirement as it is the only SQL database that is in use at Bity. \cite{bity}

The data that is being gathered is either regarding transactions or peers. Therefore the two have been split up into two separate tables which includes all the transactions and peers for each node.

This tool will be running on more than one cryptocurrency so for the sake of simplicity, all observed transactions and peers have been added to a single table.

#### Status
The possible statuses of a transaction are defined as a set of ENUM, limiting the allowed values to those specified below.
```
CREATE TYPE status AS ENUM ('pending', 'removed', 'failed', 'success');
```
\begin{description}
\item[Pending] The default value that all transactions are given when first observed. These transactions have yet to be added to a block.
\item[Removed] When a pending transaction has been removed from the mempool to free up space before being added to a block.
\item[Failed] When a transaction has an incorrect signature or in the case of Ethereum if the transaction gas consumption exceeds the blocks gas limit.
\item[Success] When a transaction has been added to the block and been processed.
\end{description}

#### Transactions
```
CREATE TABLE transactions (
  id                serial PRIMARY KEY,
  hash              varchar(80),
  current_status    status,
  time_observed     int NOT NULL,
  time_completed    int,
  currency          varchar(15) NOT NULL,
  node_id           int,
  UNIQUE            (hash, node_id)
);
```

\begin{description}
\item [ID:] the ID is automatically incremented by the database and is a unique value.
\item [Hash:] this is the hash of the transaction.
\item [Current Status:] the status is by default set to `Pending`, but when a transaction has been added to a block it is updated to `Success` or `Failed`.
\item [Time Observed:] the time at which the node observed the transaction.
\item [Time Completed:] the time that the block containing the transaction was mined.
\item [Currency:] which currency this transaction belongs to---ETH, BTC, etc.
\item [Node ID:] the ID of the node that observed it. This allows two different nodes of the same currency to add the same transaction to the database.
\item [Unique:] this field specifies that there can be no transaction with the same hash and node id. Since multiple nodes will most likely see the same transactions, we only add one of the same transaction per node.
\end{description}

#### Peers
```
CREATE TABLE peers (
  id                serial PRIMARY KEY,
  name              varchar(125) NOT NULL,
  remote_address    inet NOT NULL,
  as_number         varchar(8),
  connection_start  int NOT NULL,
  connection_end    int,
  currency          varchar(15) NOT NULL,
  node_id           int
);
```

\begin{description}
\item [ID:] the ID is automatically incremented by the database and is a unique value.
\item [Name:] the name of a node includes what software is being run, and it is version.
\item [Remote Address:] the remote address of that peer.
\item [AS Number:] autonomous system number which helps to identify what network the IP address originates from.
\item [Connection Start:] the timestamp of when the peer was first observed.
\item [Connection End:] the observed timestamp of when peer disconnects.
\item [Currency:] which currency network this peer belongs to.
\item [Node ID:] the ID of the node that observed it.
\end{description}

```
CREATE UNIQUE INDEX unique_connected_peer
  ON peers (name, remote_address, currency, node_id)
  WHERE connection_end IS NULL;
```
The `unique_connected_peer` index ensures that there is never more than one peer that is still connected with the same name, remote address, currency and node ID. Therefore the same node may only enter a connected peer in once until it is marked as disconnected, then a new entry for that peer will be made when it reconnects.
With more nodes adding peers to the database, it is possible to get multiple of the same peers. However, their node ID will be different and therefore will be added to the database.
```
CREATE UNIQUE INDEX unique_disconnected_peer
  ON peers (name, remote_address, connection_end, currency, node_id)
  WHERE connection_end IS NOT NULL;
```

This unique_disconnected_peer index ensures that there are no two peers with the same name, remote address, connection end time, current and node ID.

### Configuration
Each node needs to have two copies of the configuration file, one for peers and one for transactions to ensure that data from each is added to the database in their respective tables.

```
Title = "Config Title"

[Database]
Server = "server address"
User = "username"
Password = "password"
DbName = "database name"

[Node]
ID = #
Ticker = #

[Connect\gls{rpc}]
Host = "http://localhost:port"
```

The configuration file is divided into three parts excluding the title which is for personal reference only. The first is `Database` which includes the server address to connect to the database, by default, this is localhost if the script is running on the same machine as the node or an actual address is needed if not. The `DbName` which is the name of the database being connected to, as well as the `Username` and `Password` so that the tool can connect to the database as a user with read and write privileges.

The `Node` section contains node-specific configurations. The `ID` can be any number that will be used to identify the node. `Ticker` which specifies how often the tool asks the node for transactions and peers. Moreover, the `Currency` which is an abbreviation of the currency name that this node is associated with.

Finally the `Connect\gls{rpc}` only contains the section `Host` which refers to the address to connect to the node via \gls{rpc}. If the tool is being run on the same machine as the node, then this should be localhost followed by the port. If not then an address needs to be specified.

### Tickers
Go uses Timers and Tickers to execute code once, repeatedly or in the future. To collect data at regular intervals, the tool makes use of Tickers to poll the various blockchain networks for information repeatedly.

Tickers in Go take advantage of Go's concurrency by using channels which allows them to all participate together. This means that it is possible to have more than one Ticker going off simultaneously which continue to run until explicitly told to stop.

### Transactions
Using the \gls{rpc} calls specific to that node, and the tool can gather a list of all pending transaction hashes.
These incoming transactions are extracted into a list which returns further information about the individual transactions including their unique hash identifier. The tool adds this list of pending transactions observed by the node into an array and declares a UNIX timestamp to represent the time of observation.

The collected list of hashes and their observed time are then converted into a 'Transaction' type which consists of the hash, observed time, current status, currency associated to it---i.e. Ethereum, Bitcoin, etc.---and the ID of the node that observed it. This is done to guarantee that all transactions observed through different currencies, will have the same format. The current status is by default set to 'Pending'.

The Go SQL library used does not support passing lists of variables as SQL parameters. Therefore it is necessary to pass the query a string with the variables in the correct order and separated by (,) and grouped inside of parenthesis. By using a string builder, it is then possible to add this string and pass the whole thing to the `INSERT` query.

```
func InsertTransactions(pendingTxs []types.Transaction) {
    if len(pendingTxs) == 0 {
        return
    }
    buffer := bytes.NewBufferString("INSERT INTO transactions (hash, time_observed, current_status, currency, node_id) VALUES ")
    for i, tx := range pendingTxs {
        if i > 0 {
            buffer.WriteString(",")
        }
        buffer.WriteString(tx.TxAsSqlValue())
    }
    buffer.WriteString(" ON CONFLICT DO NOTHING")
    _, err := db.Exec(buffer.String())
    core.CheckErr("Could not insert transactions: ", err)
}
```

The string builder uses the function below---`TxAsSqlValue()`---to convert the transaction of type Transaction into a list of strings surrounded by parenthesis.

```
func (tx Transaction) TxAsSqlValue() string {
    return fmt.Sprintf("('%s', '%d', '%s', '%s', '%d')", tx.Hash, tx.ObservedTime, tx.Status, tx.Currency, tx.NodeID)
}
```

Once the transactions have been added to the database, the tool fetches the next block to be solved and the latest solved block---the pending block and the most recent block. The transactions included in the latest block are extracted into a new array of hashes which are iterated over and used to extract further transaction information. This returns the timestamp of when the block was completed as well as the current status for each of the transactions---0x1 for completed or 0x0 for failed. As different currencies will have different status codes they are passed through a function which converts the status code into either `Success` or `Failed`. If the transaction exists in the database, the status is then updated, and they are assigned the blocks time of completion.

Below is the `UPDATE` query made to the database. Unlike with `INSERT`, only one entry can be updated at a given time and therefore each transaction that needs updating is a separate query.
```
func UpdateTransaction(hash, currency string, status core.Status, time int64) {
    sqlStatement := `UPDATE transactions SET time_completed = $1, current_status = $2 where time_completed IS NULL AND hash = $3 AND currency = $4 AND node_id = $5`
    _, err := db.Exec(sqlStatement, time, status, hash, currency, nodeId)
    core.CheckErr("Could not update transactions: ", err)
}
```
### Peers
A list of all connected peers can be acquired by using specific \gls{rpc} calls which return all currently connected peers, the node's software and version, remote address and more. When a peer connects for the first time, the tool declares an observed time of connection for that peer. The peers are then converted into a `Peer` type which consists of the name---software and version---, the remote address, \gls{as} number associated to the IP range, observed timestamp upon connection,  a cryptocurrency associated to it, the id of the node that observed it. These functions are currency-specific and will be seen later in the paper.

The tool will continuously fetch an updated list of all connected peers so that if there is a new peer it is added and when a peer disconnects it is updated. This is done by checking if peers have been disconnected first and then adding the new entries.

```
func UpdatePeers (time int64, currency string, peers []types.Peer) {
    setDisconnectionTime(time, currency, peers)
    setConnectionTime(peers)
}
```
Disconnections are handled by comparing the list of connected peers to a list of all peers in the database without a disconnection time. First, the script queries the database for a list of all peers that have not been marked as disconnected and therefore where `connection_end` is null and that are not a part of the newly gathered list of connected peers.

The SQL query is split so that the individual peers can be inserted as a list of strings. There is a condition to ensure that a comma is added between each peer if more than one peer is being added to the query.
```
func setDisconnectionTime(time int64, currency string, connectedPeers types.Peers) {
    buffer := bytes.NewBufferString("SELECT id FROM peers WHERE connection_end IS NULL AND NOT remote_address IN ('")
    for i, peer := range connectedPeers.GetAddresses() {
        if i > 0 {
            buffer.WriteString("','")
        }
        buffer.WriteString(peer)
    }
    buffer.WriteString("')")
```
What was returned from the `SELECT` query is added to the variable `buffer` which if it contains nothing then no peers have disconnected and nothing happens. However, if it returns at least one row then that peer is updated with the observed time of disconnection.
```
    rows, err := db.Query(buffer.String())
    if err != nil {
        return
    }
    defer rows.Close()
    for rows.Next() {
        var id int
        err := rows.Scan(&id)
        core.CheckErr("No rows returned:", err)

        sqlStatement := `UPDATE peers SET connection_end = $1 WHERE currency = $2 AND connection_end IS NULL AND id = $3`
        _, err = db.Exec(sqlStatement, time, currency, id)
        core.CheckErr("Could not set disconnection times: ", err)
    }
}
```
Once peers have been marked as disconnected, the new peers are added to the database. Once again formatted using a string builder to insert more than one entry at a time.

The `INSERT` query has an `ON CONFLICT` statement so that if there is already a peer with the same name, remote address, \gls{as} number, observed connection time, currency and node ID, then that peer will not be added.
```
func setConnectionTime(connectedPeers types.Peers) {
    if len(connectedPeers) == 0 {
        return
    }
    buffer := bytes.NewBufferString("INSERT INTO peers (name, remote_address, as_number, connection_start, currency, node_id) VALUES ")
    for i, peer := range connectedPeers {
        if i > 0 {
            buffer.WriteString(",")
        }
        buffer.WriteString(peer.PeerAsSqlValue())
    }
    buffer.WriteString(" ON CONFLICT DO NOTHING")
    _, err := db.Exec(buffer.String())
    core.CheckErr("Could not set connection times: ", err)
}
```
The `PeerAsSqlValue()` function which is responsible for taking the passed Peer type and creating a string with the required data for that Peer.

```
func (peer Peer) PeerAsSqlValue() string {
    return fmt.Sprintf("('%s', '%s', '%s', '%s', '%s', '%d')",
        peer.Name,
        peer.RemoteAddress,
        peer.ASNumber,
        peer.ConnectionTimestamp,
        peer.Currency,
        peer.NodeID)
}
```
This is done over and over again until all peers have been added to the SQL query and then executed.
