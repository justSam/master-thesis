# Literature Review
## Cryptocurrencies, decentralised peer to peer networks
Cryptocurrencies are the result of years of people attempting to create an improved digital cash system. Digital cash is a currency that is stored and available in a digital form, unlike coins and banknotes which are physical. One of the most significant challenges was how to prevent users from replicating units of currency which would allow them to spend the same unit of currency multiple times. Physical cash makes use of a centralised trusted party such as a central bank, serial numbers, watermarking and even holograms to make it extremely difficult to replicate.

There have been many attempts in the past to create digital currencies, most of which failed for relying on centralised systems or third parties. Most notably a digital currency named DigiCash \cite{digicash} designed by David Chaum. DigiCash's digital currency was called Ecash, and it required a trusted central authority such as a bank and merchants willing to buy and sell the currency. The currency even solved the double spending problem mentioned above by using a form of digital signature called a blind signature \cite{blindsignatures} which made it impossible for the currency to be replicated by users. However the currency was a new concept at the time, and it failed when banks and merchants did not adopt it, and without merchants, users were unable to trade the currency between themselves \cite{earlyhistory}.

In 2008, Satoshi Nakamoto introduced Bitcoin, the first \gls{p2p} digital currency that was fully decentralised and did not require assistance from third parties. Unlike with Ecash, users do not have to rely on merchants to be able to exchange the currency. Bitcoin was the beginning of cryptocurrencies, decentralised digital cash networks that use cryptography to verify each transaction that passes through the network and to control the creation of new monetary assets.

Bitcoin was the first successful digital currency, and since then multiple other digital currencies have emerged inspired by the technology, some examples are Ethereum, Litecoin and Dogecoin.

### centralised, distributed and decentralised networks
A centralised network is one where users are connected to a central entity which controls the network. For instance, each client node is connected over the network to a central server that processes their requests. This can be used for computing, storage and much more \cite{centvsdecent}.

A distributed network, on the other hand, implies that the processing of information on the network is shared across multiple nodes in the network. Similarly to a centralised network, it can have a central entity that is responsible for making decisions regarding the network. The nodes participating do not store all the system information but only a portion and only act based on instructions given by the central node. For instance, a clustered distributed database could have a centralised entity responsible for managing the network and distributing tasks to nodes that work together.

A decentralised network is a subset of a distributed network. It is not managed by a single central entity and does not have a centralised server. Instead, it relies on the interaction between multiple nodes in the network which act as both client and server to reach a consensus.

A \gls{p2p} network is a subset of distributed and decentralised networks. A \gls{p2p} network is made up of a group of nodes that are each responsible for storing and sharing data through the network.  

To summarise in a centralised or distributed network, there is one---or in some cases a few---source(s) of truth which simplifies the extraction and analysis of data. In a \gls{p2p} and fully decentralised network the source of truth is shared using a consensus algorithm amongst all nodes, thus removing a node does not affect the ground truth of the network making it more robust to faults and resilient to attacks. However, no single node is aware of the network topology so, in order to properly be able to analyse the decentralised and \gls{p2p} networks, there would need to be a centralised server to collect and analyse the observable data. From this data, it is then possible to infer further information about the network topology.

## The Blockchain
A cryptocurrency is built on a technology called the blockchain. The blockchain is comprised of a sequence of blocks. Each block is comprised of a list of transactions and a header containing a unique identifier and a reference to its parent block. The blockchain is built on top of a \gls{p2p} network and is primarily used as a means to distribute information about the shared state across the network. Put simply it is a distributed and append-only ledger or database which all the peers in the network collaborate to maintain.

Nodes will always consider the longest chain as the valid one. For example, if two were to broadcast different blocks at the same time, either block could be received first. Each node will work on the first block they receive causing there to be two blocks being worked at the same time. The chain forks into two branches so that the first node to be able to compute the hash of the next block can append it to that branch and increasing its length. At this point all the nodes that were working on the other branch---which is now the shorter of the two will switch to the longer one, discarding all the work done on the other branch and therefore "orphaning" the block on the shorter chain.

\begin{figure}[ht!]
\includegraphics[scale=0.4]{./images/forks.png}
\caption{A representation of how a fork due to two blocks propagating in the network at once is handled.\cite{forkimg2}}
\end{figure}

Blockchains attempt to create a secure and trusted system through encryption, hashing and network consensus which is explained in the subsections below.

### Achieving Consensus
#### Proof of Work
Mining nodes validate and include transactions, whose validity has also been verified by the rest of the network, to a block. Blocks are then added to the chain which the miner believes to be valid, the rest of the network will then select the longest chain reaching a consensus which most commonly is established using proof of work \cite{pow}. Where "[...] the weight of a single node in the consensus voting process is directly proportional to the computing power that the node brings." \cite{ethwhitepaper}

Miners compete in computing the hash for the block until they are able to find the solution. The difficulty comes from how many zeroes the hash must begin with to be valid. If blocks are being added too quickly the difficulty increases by adding a new zero, if instead they are being added too slowly then a zero is removed to decrease the difficulty which occurs approximately every two weeks. This will vary depending on how many people are actively mining and their hashing power. The first to solve the problem by finding a hash with the correct number of zeroes---in other words, the first to mine the block---receives a reward for its trouble. This corresponds to a certain amount of newly generated currency as defined in the protocol as well as all the fees for all the transactions included in the block.

To summarise, proof of work involves many miners trying to solve complicated problems via brute force while they compete with each other to add the next block in the chain. The first node to solve it receives the reward once the block has been broadcast through the network and validated by other nodes. This is the primary consensus algorithm being used by blockchains at this point, however, other algorithms may become alternatives in the future.

#### Proof of Stake
A system using proof of work requires much energy by design to enforce a cost on miners that participate thus preventing attacks such as Sybil attacks.

Proof of stake \cite{powpos} is a proposed alternative that does not require a problem to be solved via brute force but instead a validator known as a forger. The forgers are selected depending on their wealth referred to as their stake. Anyone that owns some of the currency is eligible to become a forger.

Ethereum is an example of a cryptocurrency that is planning on switching from proof of work to a proof of stake algorithm called Casper. The protocol is still under active development.

"In PoS-based public blockchains (e.g. Ethereum's upcoming Casper implementation), a set of validators take turns proposing and voting on the next block, and the weight of each validator's vote depends on the size of its deposit (i.e. stake). Significant advantages of PoS include security, reduced risk of centralization, and energy efficiency." \cite{ethpos}

There are slashing conditions that determine whether validators have been creating or voting for multiple invalid blocks. "If a validator triggers one of these rules, their entire deposit gets deleted." \cite{ethpos}

#### Proof of Authority
Proof of authority differs greatly from proof of work and proof of stake as it by definition, not a decentralised consensus algorithm \cite{poa, poaprivate}.

A network using the proof of authority consensus uses authoritative validators to validate blocks and transactions. These validators must earn their right to validate which can only be obtained by having a registered identity that can be cross-checked publicly. The validation of blocks and transactions has to be accepted by the majority of validators thus making it difficult for non-honest nodes to interfere with the network, that does not, however, mean that it can not be done.

This consensus algorithm is often used for private chain setups where block issuers identities are centralised and can easily be held accountable, similarly to a whitelist.

### Node functions
Nodes in a blockchain network can take on different functions within the network or can even do all of them \cite{nodefunctions}. These are validating and mining. The routing function is not included in that list since a node requires it in order to participate in the network, and therefore all nodes include it. All nodes are responsible for maintaining their connection to peers as well as verifying and distributing blocks and transactions through the network.

Validating is a function that can be done by full that can maintain a complete copy of the current state of the blockchain in a local database. The local copy enables a node to verify transactions without the need for any external information. \gls{spv} or Lightweight nodes on the other hand only maintain a chunk of the state of the blockchain.

Mining nodes can be full nodes or lightweight nodes, and they compete to create new blocks using the proof of work algorithm. Lightweight nodes can mine as a part of a mining pool which includes other mining nodes that work together and are dependent on at least one full node.

A peer may perform any or all of these functions. Some nodes can be used for exchange services where users can buy and sell cryptocurrencies or block explorer services where users can search for transactions, addresses and blocks.

### Mining
Without a central authority, transactions in the network are included into blocks by entities called miners \cite{ethwhitepaper, btcwhitepaper}. Mining is designed to be challenging and therefore resource-intensive activity. Thus miners are given a monetary incentive in the form of a reward for validating transactions and adding the next block in the chain. The difficulty of the block is determined by the network and changes when new blocks are being added too quickly or too slowly. Ensuring that the number of blocks that can be processed is steady over time.

As cryptocurrencies do not have a centralised authority, each peer participating in the network is responsible for processing every transaction individually and communicating their observations through the network. A consensus algorithm ensures that no one node can make a change to the blockchain without reaching a consensus with other nodes in the network.

In order to compute the next block, miners have to find a hash for the block with specific properties, such as a particular number of zeros at the beginning of the hash. To achieve this result, the miner is allowed to change a specific field of the block---a nonce. When a hash is found that matches the expected property, the new block can be added to the chain. The hash function used is cryptographically secure which means miners are forced to use brute force---and thus trying multiple values of the nonce---to find a valid solution. The likeliness for a miner to find the solution is proportional to their computational power. Some miners pool their resources to increase their chances of finding a valid solution and then split the reward.

### Distributed ledger
Each peer participating in the network is responsible for processing every piece of information individually and communicating their observations through the network \cite{distledger}. To accomplish this, each node has its copy of the distributed ledger which contains all the transactions that have been included into a block and have therefore been executed. In the case of cryptocurrencies, this includes the ownership status of all existing units of currency.

The ledger is append-only and may only be updated if there is a consensus between the participating nodes in the network. Once there is a consensus between nodes, the ledger is updated, and each node maintains an identical copy locally preventing exploits such as double spending and the falsification of records so long as honest nodes make up the majority of the network.

### Blocks and Transactions
The structure of a block can vary between cryptocurrencies but very generally they all contain a header and a list of transactions. The block's header contains a pointer to the block before it and a payload to confirm that the data within the block has not been altered. The hash value of the block is calculated by miners using a standard algorithm based on the block's data. This hash is used as a unique identifier for the block. It is possible to recalculate the hash to confirm that the data has not been altered, but there is no way to retrieve the block's data using the hash because of the one-way properties of the block hash function \cite{masteringBTC}.

The blockchain can only be secure if it is immutable, such that no block in the chain can be modified. If a previous block is modified, it will invalidate the entire chain from that block onward. Alternatively, any attempt to modify a block without invalidating the chain would require recomputing the hash of every block that follows it. Recomputing the hash of very block is computationally infeasible for a single entity especially considering that the network continues to add new blocks.

All blocks contain a header, hash of the previous block header and a list of transactions which is a list of all of the transactions that have been included in that block.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.1]{./images/blocks.png}
\caption{Simplified view of blocks in a chain \cite{btcblockimg}.}
\end{figure}

The blockchain uses public-key cryptography, with which users can generate wallets and sign transactions. Public-key cryptography consists of two keys, a private key which should only ever be known by the user and a public key used to create a public address known by all. The public address allows the user to receive funds. The private key is used to sign transactions to ensure that the origin of the transaction is valid---i.e. to ensure that the user controls the addresses generated by the public key.

These transactions contain the public addresses of both the sender and the recipient, the quantity of currency to send and the transaction fee. The transaction fee is the cost to process the transaction and is given to the miner including the transaction in a block.  The fee can be adjusted in order to hasten the inclusion of that transaction into a block. Miners can decide which transactions to include or not to include and usually accept transactions with the higher transaction fee and thus the highest return for their work. To put it simply the network behaves as follows: \cite{btcwhitepaper}

\begin{enumerate}
\item New transactions are broadcast to all nodes in the network.
\item Mining nodes collect transactions and add them to a block.
\item Mining nodes attempt to find a hash for the block.
\item When a mining node succeeds, the block is broadcast to all other nodes.
\item Validating nodes verify the validity of the block by ensuring that all transactions are valid.
\item If there is an agreement between nodes, the block is added to the chain. If not then the block is rejected.
\end{enumerate}

### Merkle Trees
The information in this section is based on \cite{merkleeth, merkleimg}.

Cryptocurrencies make use of Merkle trees which hash all of the transactions present in the block in pairs until only one hash remains called the Merkle root. The hashing is done in pairs to ensure that no transaction can be changed without affecting the rest of the tree including the final Merkle root. To verify a transaction it is then possible to use the Merkle root and follow the transactions branch on the tree. This is especially useful for thin or light clients to determine the status of a transaction.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.5]{./images/merkleroot.png}
\caption{Example of a Merkle tree.\cite{merkleimg}}
\end{figure}

Bitcoin, for instance, uses Merkle proofs to allow thin clients to determine the status of a transaction described in Bitcoin's white paper as a \gls{spv}. Merkle proofs help to prove whether a transaction has been included into a block but does not give any indication about the current state of the network.

Ethereum uses multiple Merkle trees to surmount this problem. The trees store the list of transactions, their receipts containing data of the transactions and state. A change in state occurs when new accounts are being created, balances being altered, etc.

Binary Merkle trees are used to store lists of data, therefore can easily handle the storing of transactions and receipts without a problem. However, to store the state requires a key-value mapping which must be updated often. In a binary Merkle tree, changing any value would make the rest of the tree invalid so to avoid having to recompute the tree each time the state is changed Ethereum uses a Merkle Patricia Trie also known as radix tree. A radix tree is a tree data structure used to store key-value mappings that can be modified as the key is the path, to the corresponding value, through the tree \cite{merkleimg}.

### Forks in the network
The information in this section is based on \cite{hardvssoft, chainsplit}.

All nodes are supposed to validate blocks using the same consensus rules. However, when the consensus rules are modified to introduce new features or increased security, not all nodes upgrade immediately. When nodes don't upgrade, it is possible to have multiple nodes using two different set of rules. This can cause a few issues, for example, if a new feature has been added to transactions or blocks, upgraded nodes will consider them valid while non-upgraded ones will reject them.

When this occurs, the network can fork. There are two different kinds of forks, hard forks and soft forks.
A hard fork occurs when there is a change in the protocol that makes all nodes running older versions of the protocol invalid. Using the example above where non-upgraded nodes consider new blocks invalid, they will continue to create blocks and add them to the previously valid block using the old protocol. This leads to there being two branches, one running the old protocol and one running the new one.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.7]{./images/hard-fork.png}
\caption{A hard fork that causes the blockchain to split into two branches. \cite{forkimg}}
\end{figure}

Hard forks can cause complications as any unit of currency spent on one branch can then be spent again on the other. A possible solution involves abandoning one of the branches entirely or in Bitcoin's case, a hard fork led to a split where one branch continued as Bitcoin Core and the other as the newly created Bitcoin Cash.

Soft forks, on the other hand, occur when there is a change that adds stricter rules to blocks and transactions. For example, a change that makes rules stricter in the new protocol implies that nodes running the old protocol with more flexible rules will accept blocks made using the new protocol. However, the updated nodes will not accept those made using the old protocol as they violate the new rules. Ensuring that the new branch grows faster than the old one. As miners realise that updated nodes are not accepting their blocks, they are forced to upgrade as well, and the old branch is eventually abandoned \cite{hardvssoft}.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.7]{./images/soft-fork.png}
\caption{An example of a soft fork.\cite{forkimg}}
\end{figure}

#### UASF and UAHF
UASF or User Activated Soft Fork is a mechanism where a soft fork is specified to occur on a specific date and is enforced by the economic majority. An entities weight in the economic majority is largely based on that entities ability to devalue the coins in the protocol version they oppose \cite{econmajor}.

UAHF or User Activated hard Fork is when developers add a set of rules that change the node software making previously invalid blocks valid after a specific date.

Bitcoin has had some known weaknesses for a while, so a solution to these problems was proposed called \gls{segwit} \cite{segwit}. \gls{segwit} is an update for Bitcoin Core which aimed to solve Bitcoin's transaction malleability problem. Transactions in bitcoin are signed, however not all of the data is hashed to create the transaction hash. Which means it is possible to change some of the Transaction data without invalidating it.

As bitcoin is a decentralised \gls{p2p} network, the activation of SegWit was highly dependant on whether or not the economic majority was supporting the activation or not. In 2016 and 2017 the activation of \gls{segwit} was blocked by miners who disagreed with the changes. However, the economic majority was in favour of the changes and created a UASF bypassing the miners and activating \gls{segwit} soon after the 1st of August 2017 \cite{segwit}.
