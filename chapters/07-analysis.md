# Data Analysis
The data present in this chapter has been collected exclusively from the Ethereum and Bitcoin networks. This data was gathered from multiple nodes set up in geographically different locations, as of right now there are a total of three nodes running Ethereum located in Toronto, Singapore and Bangalore and one node running Bitcoin in Switzerland. The Ethereum nodes are being run using DigitalOcean while the Bitcoin node is using Exoscale---computing platforms \cite{digitalocean, exoscale}. These nodes have been collecting data and inserting it into a centralised database located in Switzerland.

## Degree of Decentralization
### Hypothesis
The cost of running a full node at home will cause there to be an increased number of users that run their nodes with more cost-effective solutions. This is inadvertently causing nodes to cluster under the same services leading to the network being less decentralised which is the opposite of the goal of a decentralised system.

### Data Visualization
To be able to gain a clearer picture of the network topology, the data used in the following graphs is the list of \gls{asn}s extracted from unique observable peers. A peer is considered unique if it has a different IP address from other observed peers.

#### Ethereum
The graph in figure \ref{fig:eth-bar} shows the total number of nodes that are being run on the same \gls{as}s. A large portion of these observed \gls{asn}s contains a small number of connected nodes which form a long tail which has been aggregated to improve the readability of the graph. The long tail is marked in a lighter colour and consists of \gls{asn}s with fewer than 25 nodes.
\pagebreak
The bar chart displays Amazon as the web service with the greatest number of nodes running on its \gls{as}. The next reoccurring \gls{as}s are from Alibaba, Google and DigitalOcean.

The bar graph in figure \ref{fig:eth-bar} demonstrates how many nodes there are per \gls{as} however it does not provide an accurate representation of the subdivision of the network primarily because the \gls{asn} with fewer nodes had to be aggregated.
Therefore, the pie chart in figure \ref{fig:eth-pie} which contains the same data without any aggregations can give a clearer idea about the division of \gls{as}s observed in the network.

The graph shows that Amazon, Alibaba, Google and DigitalOcean are not just the most popular web services, but they are also hosting more than 51% of all observed nodes. This distribution of observed nodes per \gls{as} demonstrates that the Ethereum network is significantly more centralised than it should be.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/eth-peer-bar}
\caption{Bar chart representing the number of nodes associated to an \gls{asn} in the Ethereum network.}
\label{fig:eth-bar}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.3]{./images/eth-peer-pie}
\caption{Pie chart representing the distribution of nodes per \gls{asn} in the Ethereum network.}
\label{fig:eth-pie}
\end{figure}

In this instance, it would be possible to disrupt the network by taking down the four main web services which are significantly more accessible than tracking down thousands of individual nodes to shut down.
\pagebreak

#### Bitcoin
\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-peer-bar}
\caption{Bar chart representing the number of nodes associated to an \gls{asn} in the Bitcoin network.}
\label{fig:btc-bar}
\end{figure}

The graph in figure \ref{fig:btc-bar} reveals that in Bitcoin's case there is only one web service with a significant portion of users running nodes on its \gls{as}s. The aggregated \gls{as}s with fewer than 14 nodes appear to outnumber Google thus making the network considerably harder to manipulate.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-peer-pie}
\caption{Pie chart representing the distribution of nodes per \gls{asn} in the Bitcoin network.}
\label{fig:btc-pie}
\end{figure}

Figure \ref{fig:btc-pie} reveals a clearer image of the state of decentralisation of the Bitcoin network. It would appear that the majority of nodes are concentrated in 10 web services which do indicate an element of centralisation considering that taking down those ten services could potentially disrupt the network.

### Analysis
It is important to note that the data that has been collected is limited to the reachable portion of the Bitcoin and Ethereum networks as defined in chapters \ref{the-bitcoin-network} and \ref{the-ethereum-network}. This means that there is a limitation to how much of the network can be observed, the current analysis has only been performed with a few nodes. Increasing the number of nodes would allow for a more significant portion of the network to be observed and help depict a more accurate picture of the network. The unreachable portion of the network is connected to the reachable portion and can, therefore, be disrupted if something were to disrupt the reachable portion potentially fragmenting the network.

From the graphs above, we see that the majority of observable nodes have gravitated towards third party hosting solutions. This could be the result of better and more cost-effective solutions---provided by web hosting organisations---for running a node. An excellent example of this is the nodes that are currently up and running as part of the Node Explorer tool that is gathering this data. The three Ethereum nodes are all hosted by DigitalOcean as it is a convenient solution, further contributing to the centralisation of the network. This is not surprising as these large service providers can provide users great services at a reduced cost.

When the graphs for Ethereum and Bitcoin are compared, it appears that Bitcoin has a significantly better distribution of nodes. This could be due to the mistrust that the Bitcoin community has regarding third party services such as banks and large tech companies.

From the data, we can see that in order to further decentralise the Ethereum and Bitcoin networks it would be enough to relocate the top ten web hosting services to achieve a higher level of distribution. This is demonstrated in figure \ref{fig:eth-pie-decent} and figure \ref{fig:btc-pie-decent}.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/eth-peer-decent}
\caption{Example of node distribution if the Ethereum network were to relocate nodes using the top ten web hosting services.}
\label{fig:eth-pie-decent}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-peer-decent}
\caption{Example of node distribution if the Bitcoin network were to relocate nodes using the top ten web hosting services.}
\label{fig:btc-pie-decent}
\end{figure}
\pagebreak

## Network Reliability
### Hypothesis
In a reliable network, it would be expected that there is more than a single software implementation for a given network where there are multiple software versions following the same protocol. If many nodes run the same version, then a flaw in the specific version could bring a significant portion of the network to a halt. However, if there are too many different versions following different protocols, this could lead to the fragmentation of the network.

### Data Visualization
The data for this metric includes the node software, and it is respective version numbers.

#### Ethereum
In the Ethereum community, there are two popular node software: Geth and Parity.
There are other

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/eth-ver-pie}
\caption{Pie chart representing the total number of nodes running different Ethereum node software.}
\label{fig:eth-ver-pie}
\end{figure}

Displayed in figure \ref{fig:eth-ver-pie}, it is clear that amongst observed nodes Geth is the preferred node software. The reason for this being that Geth is the official node software maintained by the Ethereum Foundation. In figure \ref{fig:eth-geth} is a small snapshot of the versions of Geth that nodes are running.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/eth-geth}
\caption{Snapshot of Geth's observed version numbers.}
\label{fig:eth-geth}
\end{figure}

In figure \ref{fig:eth-parity} is the observed node software versions of Parity.

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/eth-parity}
\caption{Snapshot of Parity's observed version numbers.}
\label{fig:eth-parity}
\end{figure}

Looking at both figure \ref{fig:eth-geth} and \ref{fig:eth-parity} there does not appear to be a significant difference, but that is primarily because \ref{fig:eth-geth} is only a small portion of the data. Geth appears to be considerably more fragmented to Parity this may be due to a few things. There is a much more significant number of users running a Geth node which means there is a higher chance for human error and failure to update nodes. Finally, Parity has an automatic updating feature which could explain why there are fewer versions in circulation. However, updating each node with the auto-updater could be dangerous as a flaw or bug may be propagated to many nodes automatically which may significantly deteriorate the network.

#### Bitcoin
To connect to the Bitcoin network, there are many more options regarding node software. This could be because Bitcoin is an older blockchain that has been in circulation for almost ten years. The two most common are bitcore and bitnodes.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.25]{./images/btc-ver-pie}
\caption{Pie chart representing the total number of nodes running different Bitcoin node software.}
\label{fig:btc-ver-pie}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-bitcore}
\caption{List of observed versions of Bitcoind.}
\label{fig:btc-bitcore}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-bitcoinj}
\caption{List of observed versions of Bitcoinj.}
\label{fig:btc-bitcoinj}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{./images/btc-satoshi}
\caption{List of observed versions of Satoshi.}
\label{fig:btc-satoshi}
\end{figure}

\pagebreak
### Analysis
While the nodes on the network appear to be decentralised, they all rely on the same node software, but not necessarily the same version. Node software is still under active development and is quite often updated, however not every node updates and not at the same time. This can lead to there being multiple different versions of the same protocol on the network at any given time.

A concentration of nodes using the same software over another could be problematic as a bug in that software could affect a majority of the nodes. Ideally, there should be a few different node implementations with equal shares of nodes on the network, such that a bug in one software only has a limited impact on the network. This will ensure that if something were to happen to one implementation, the network can switch to another until the issue has been resolved.

If the network is reliable, then it would be expected that there is more than a single software implementation for a given network. If there are too many different versions following different protocols, this could fragment the network. However, if all versions are different but all following the same protocol it could stop a flaw in one version from tearing down the network.

Ethereum has less node software but considerably more versions compared to Bitcoin that has more node software and fewer versions. Having too many software versions can become a problem as they need to be continuously maintained.

## Transaction Propagation
The geographical location, the quality of the link and the potential underlying traffic analysis by network owners may have an impact on the time it takes for a node to observe incoming transactions. Transaction propagation is calculated by taking the times at which two nodes observe the same transaction and comparing the difference between the two.

### Data Visualisation
\begin{figure}[ht!]
\centering
\includegraphics[width=\linewidth]{./images/eth-tx}
\caption{Ethereum transaction propagation.}
\label{fig:eth-tx}
\end{figure}

The label on the X axis reppresents which node was able to observe a transaction before the other. For instance Canada then Singapore refers to the node based in Canada observing a transaction before the node in Singapore. The difference between the observed times of both nodes makes up a single point in the graph. The Y axis on the other hand reppresents the time in seconds.

\pagebreak

### Analysis
This metric can only be visualised for Ethereum as it requires at least two nodes so that transaction observation times can be compared to each other.
In figure \ref{fig:eth-tx} there are three nodes, one in Singapore, one in Canada and one in India. The times at which these nodes observed transactions are compared so that it is possible to get an idea of how well connected the nodes are to the network.

The time at which nodes observe the same transaction, presented in figure \ref{fig:eth-tx}, appears to range between 0 and 800 seconds. This data however is inconclusive as communication throughout a network should be in the order of tens to hundreds of milliseconds and not tens to hundreds of seconds.

## Problems Regarding Data Collection
### Incomplete Tool
The Node Explorer tool is still under development and therefore requires a lot more work to be able to ensure that the data that has been observed is a reflection of the network. An example of this can be seen from the graph in figure \ref{fig:eth-tx} where the results appear to be flawed.

### Limited Sample Size
I initially began this project with one Ethereum and one Bitcoin node. Using these nodes, I was able to successfully program the Node Explorer tool and test that every aspect worked correctly. However, this thesis is not only about the collection of data but about the analysis.

As a result, I found the data from one node to be limited as I am only able to see a snapshot of the network that way. In order to be able to interpret the data and analyse it, I needed more snapshots of the network.

Due to the Bity's infrastructure policies, I have no direct access to the machines running my tool. This makes it a lot more complicated to run and monitor the program.

### Validity and Reliability
The data regarding versions and \glspl{as} tends to be reliable as people tend to leave their nodes where they are and do not migrate them. Therefore they remain within the same \gls{as}. Regarding versions, existing nodes do not tend to update too regularly, and as new nodes are deployed to the network, they usually include the latest version thus increasing the likelihood of fragmented versions.
